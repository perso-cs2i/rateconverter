package fr.cs2i.rateconverter.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.cs2i.rateconverter.R

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // The layout
        setContentView(R.layout.activity_about)
    }
}