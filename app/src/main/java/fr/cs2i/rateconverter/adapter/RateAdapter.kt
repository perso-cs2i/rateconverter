package fr.cs2i.rateconverter.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.cs2i.rateconverter.R
import fr.cs2i.rateconverter.config.AppConfig
import fr.cs2i.rateconverter.model.Rate

/**
 * RateAdapter
 * Class that manages a collection of items
 * generates a layout for each requested item that is requested by RecyclerView
 *
 * @param context An Android context (activity or service)
 *                 (will be used to inflate the item layout)
 * @param itemLayout The XML layout of 1 item (here: item_rate.xml)
 */
class RateAdapter(
    private val context: Context,
    private val itemLayout: Int,
    private val rates: List<Rate>
): RecyclerView.Adapter<RateAdapter.ViewHolder>() {
    // ---
    // ADPATER METHODS
    // ---

    /**
     * Create a new instance of ViewHolder (a view cache)
     *
     * @param parent: The parent RecyclerView
     * @param viewType: The type of view to be created
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(context)
            .inflate(itemLayout, parent, false)

        return ViewHolder(view)
    }

    /**
     * Bind an existing ViewHolder to new values
     * @param holder A ViewHolder with child views
     * @param position The position of an item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Get a rate by its position
        val rate = rates[position]

        // Transfer rate values into child views
        holder.rateCodeView.text = rate.code
        holder.rateCurrencyView.text = rate.currencyName
        holder.rateValueView.text = "%.4f".format(rate.value) //rate.value.toString()

        // TODO Transfer flag info

        // v1
        // holder.rateFlagView.setImageResource(R.drawable.usd)
        val holderUrl = "https://rates.self-access.org/flags/"

        // v2
        /*
        Picasso.get()
            .load("https://rates.self-access.org/flags/${rate.code.toLowerCase()}.png")
            .into(holder.rateFlagView);
         */


        // v3 : final
        Picasso.get()
            .load(AppConfig.FLAG_URL.format(rate.code.toLowerCase()))
            .into(holder.rateFlagView);
    }

    /**
     * Number of items (here: rates) that are managed by the adapter
     */
    override fun getItemCount() = rates.size

    /*fun getItemCountOld(): Int {
        return rates.size
    }*/

    // ---
    // VIEWHOLDER
    // ---

    /**
     * The Rate ViewHolder
     * A buffer/cache which inflates and stores the item's child views
     *
     * @param itemView The parent view (the item view, the ConstraintLayout)
     */
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        val rateCodeView: TextView = itemView.findViewById(R.id.rate_code)
        val rateCurrencyView: TextView = itemView.findViewById(R.id.rate_currency_name)
        val rateValueView: TextView = itemView.findViewById(R.id.rate_value)
        val rateFlagView: ImageView = itemView.findViewById(R.id.rate_flag)
    }

}