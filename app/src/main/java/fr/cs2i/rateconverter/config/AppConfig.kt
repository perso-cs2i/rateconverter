package fr.cs2i.rateconverter.config

/**
 * Application configuration
 */
object AppConfig {
    // application tag
    val TAG = "RateConverter"

    // API urls
    val API_ROOT = "https://rates.self-access.org/"
    val FLAG_URL = API_ROOT + "flags/%s.png"
    val CURRENCY_API = API_ROOT + "currencies.php"
    val RATE_API = API_ROOT + "rates.php"

    // Database constants
    val DATABASE_NAME = "rates_db"
    val DATABASE_VERSION = 1
}