package fr.cs2i.rateconverter.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import fr.cs2i.rateconverter.config.AppConfig

/**
 * DataAdapter
 * Opens and closes tha database
 */
class DataAdapter {

    // ---
    // FIELDS
    // ---

    // The Database  helper
    // This varaiable is declared as "lateinit" beacause we MUST be declared here,
    // but we CAN'TR define the value right now
    private lateinit var dataHelper: DataHelper

    // The database (public to be used in other classes)
    lateinit var database: SQLiteDatabase

    // ---
    // METHODS
    // ---

    fun open(context: Context) {
        // Instaciate a DataHelper
        // (and all onCreate/onUpgrade if necessary)
        dataHelper = DataHelper(context, AppConfig.DATABASE_NAME, AppConfig.DATABASE_VERSION)

        // Open the database
        try {
            database = dataHelper.writableDatabase
        } catch (e: SQLiteException) {
            database = dataHelper.readableDatabase
        }

    }


    /**
     * Close the database (and the DataHelper)
     */
    fun close(

    ) {

    }
}