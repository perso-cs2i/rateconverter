package fr.cs2i.rateconverter.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import fr.cs2i.rateconverter.config.AppConfig
import fr.cs2i.rateconverter.io.IoUtil

/**
 * DataHelper : this class is used when tha database is opened
 * to create or upgrade the database
 */
class DataHelper(
        val context: Context,
        databaseName: String,
        databaseVersion: Int
)
    : SQLiteOpenHelper(context, databaseName, null, databaseVersion) {

    /**
     * onCreate: this method is called when the database is opened for the list time
     * (and should be created)
     *
     * @param db The database
     */
    override fun onCreate(db: SQLiteDatabase) {
        Log.d(AppConfig.TAG, "Creating initial tables...")

        // Create the rates table
        // (INTEGER + PRIMARY KEY => AUTO INCREMENT implicity)
        // The SQL command
        // val sqlRates = IoUtil.readTextFile(context, "sql/sql001sql")
        // db.execSQL(sqlRates)

        var sql = """
            CREATE TABLE rates(
            id INTEGER PRIMARY KEY,
            code TEXT NOT NULL,
            currency_name TEXT,
            rate_value FLOAT
            )
        """.trimIndent()
        Log.d(AppConfig.TAG,"Executing SQL: $sql")

        // Execute the command
        db.execSQL(sql)
    }

    /**
     * onUpgrade: this method is called to upgrade the database, when the user upgrades
     * the app itself, and when the installed version (on the device) is lower than
     * the one in the upgrade
     * "if oldVersion < newVersion -> call onUpgrade)
     *
     * @param db The database
     */
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        Log.d(AppConfig.TAG, "Upgrading database...")
    }
}