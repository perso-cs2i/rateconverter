package fr.cs2i.rateconverter.data

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf
import fr.cs2i.rateconverter.model.Rate

/**
 * The RateRepository
 *
 * @param db A database
 */
class RateRepository(val db: SQLiteDatabase) {

    val tableName = "rates"

    fun deleteOne(rate: Rate) {
        // v1: raw SQL
        // db.execSQL("DELETE FROM rates WHERE id = ${rate.id}")

        // v2: Android SQL API
        /*db.delete(
                tableName,
                "id = ${rate.id}",
                null
        )*/

        // v3: Android SQL API with params
        db.delete(
                tableName,
                "id = ?",
                arrayOf(rate.id.toString())
        )
    }

    fun deleteAll() {
        // DELETE * FROM rates;
        db.delete(
                tableName,
                null,
                null
        )
    }

    fun deleteMany(rate: List<Rate>) {
        // DELETE * FROM rates;
        /*db.delete(
                "rates",
                null,
                null
        )*/
    }

    fun insertOne(rate: Rate) {
        // Map the rate values to the fields
        // of the table
        val values = ContentValues()
        values.put("code", rate.code)
        values.put("currency_name", rate.currencyName)
        values.put("rate_value", rate.value)

        db.insert(
                tableName,
                null,
                values
        )
    }

    fun insertMany(rates: List<Rate>) {
        db.beginTransaction()

        for(rate in rates)
        {
            insertOne(rate)
        }

        db.setTransactionSuccessful()
        db.endTransaction()
    }

    fun updateOne(rate: Rate) {
        // Map the rate values to the fields
        // of the table
        val values = ContentValues()
        values.put("code", rate.code)
        values.put("currency_name", rate.currencyName)
        values.put("rate_value", rate.value)

        db.update(
                tableName,
                values,
                null,
                null
        )
    }

    fun updateMany() {

    }

    fun find() {

    }

    fun findAll() {

    }

}