package fr.cs2i.rateconverter.json

import android.util.Log
import fr.cs2i.rateconverter.config.AppConfig
import fr.cs2i.rateconverter.model.Currency
import org.json.JSONException
import org.json.JSONObject


object CurrencyConverter {

    /*fun convert(jsonCurrencies: String): MutableList<Rate> {
        // TODO
        return  mutableListOf(
                Rate(-1,"USD","Dollar", 0.0)
        )
    }*/

    /*fun convert2(jsonCurrencies: String): MutableMap<String, String> {
        var currencies = mutableMapOf<String, String>()
        currencies["USD"] = "US Dollar"
        currencies["EUR"] = "Euro"

        return currencies
    }*/

    fun convert(jsonCurrencies: String): MutableList<Currency> {
        val currencies = mutableListOf<Currency>()

        // TODO :JSONbject / JSONArray

        // Instanciate a JSONObject from the raw currencies
        val jo = JSONObject(jsonCurrencies)

        for(key in jo.keys())
        {
            val currencyName = jo.getString(key)
            //Log.d(AppConfig.TAG, "Key: $key, name: $currencyName")

            // create a currency instance
            val currency = Currency(key, currencyName)

            // add the currency to the list
            currencies.add(currency)
        }


        return currencies
    }
}