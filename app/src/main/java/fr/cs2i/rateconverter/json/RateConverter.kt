package fr.cs2i.rateconverter.json

import android.util.Log
import fr.cs2i.rateconverter.config.AppConfig
import fr.cs2i.rateconverter.model.Currency
import fr.cs2i.rateconverter.model.Rate
import org.json.JSONObject

object RateConverter {

    /**
     * Convert a JSON file/string from the API to a collection of rates
     *
     * @param jsonRates The raw JSON string coming from the API
     * @param currencies The list of currencies (coming from the CurrencyConverter)
     */
    fun convert(jsonRates: String, currencies :MutableList<Currency>): MutableList<Rate> {
        val rates = mutableListOf<Rate>()
        var rateID = 1

        // TODO :JSONbject / JSONArray

        // Instanciate a JSONObject from the raw currencies
        val jo = JSONObject(jsonRates)

        val joR = jo.getJSONObject(("rates"))

        for(key in joR.keys())
        {
            // The value
            val value = joR.getDouble(key)
            //Log.d(AppConfig.TAG, "Key: $key, value: $value")

            // extension/lamdba method to find the matching currency
            var searchCurrency = currencies.first { c -> c.code == key }

            // create a currency instance
            val rate = Rate(rateID++, key, searchCurrency.CurrencyName, value)

            //val jo2 = JSONObject(currencies.toString())

            // add the currency to the list
            rates.add(rate)
        }

        return rates
    }
}