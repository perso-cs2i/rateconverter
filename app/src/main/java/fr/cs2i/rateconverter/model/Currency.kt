package fr.cs2i.rateconverter.model

/**
 * Currency
 * This model is used as a DTO (Data Transfer Object)
 * when converting Currency API to Kotlin data
 */
data class Currency (
        var code: String,
        var CurrencyName: String
)