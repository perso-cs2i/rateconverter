package fr.cs2i.rateconverter.model


/**
 * Rate Model
 */
data class Rate(
    var id: Int,                // For SQLite
    var code: String,           // The currency code(e.g USD)
    var currencyName: String,   // Currency name (e.g United State Dollar)
    var value: Double           // Currency value
)

{
    fun flag() = "$code.png"
}

