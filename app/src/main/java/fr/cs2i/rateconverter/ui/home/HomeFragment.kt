package fr.cs2i.rateconverter.ui.home

import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.cs2i.rateconverter.R
import fr.cs2i.rateconverter.adapter.RateAdapter
import fr.cs2i.rateconverter.config.AppConfig
import fr.cs2i.rateconverter.config.AppConfig.RATE_API
import fr.cs2i.rateconverter.data.DataAdapter
import fr.cs2i.rateconverter.data.RateRepository
import fr.cs2i.rateconverter.io.IoUtil
import fr.cs2i.rateconverter.json.CurrencyConverter
import fr.cs2i.rateconverter.json.CurrencyConverter.convert
import fr.cs2i.rateconverter.json.RateConverter
import fr.cs2i.rateconverter.model.Rate
import fr.cs2i.rateconverterkt.http.HttpUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class HomeFragment : Fragment() {

    // ---
    // FIELDS
    // ---

    // The DataAdapter to have access
    private val dataAdapter = DataAdapter()

    // ---
    // LIFECYCLE
    // ---

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        // the fragment's layout
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        // Open the database connection
        dataAdapter.open(requireContext())

        // THe final view/layout
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Display rates
        displayRates()

        // Test Currencies
        //getRatesFromJson()

        // Test Rates
        //testRates()

    }

    override fun onDestroy() {
        super.onDestroy()

        // The fragment is destroying ... close the DataAdapter before
        dataAdapter.close()
    }
    // ---
    // RECYCLERVIEW
    // ---

    /**
     * Display rates on RecyclerView
     */
    private  fun displayRates() = runBlocking{
        // Retrieve rates

        // v1
        //val rates = getRates()

        // v2
        //val rates = getRatesFromJson()

        // v3

        /*
        val x = withContext(Dispatchers.Main) {
            Log.d(AppConfig.TAG, "What is the reason of ...?")
            42
        }
        */

        // 2.Passage en arrière-plan pour extraire les devises
        val ratesAPI = withContext(Dispatchers.IO) {
            // short version
            // getRatesFromAPI()

            val asyncRates = getRatesFromAPI()

            Log.d(AppConfig.TAG, "Saving rates in database...")
            val rateRepo = RateRepository(dataAdapter.database)
            rateRepo.deleteAll()
            rateRepo.insertMany(asyncRates)

            // Return the rates (we are in a lambda, we omit the RETURN)
            asyncRates
        }


        // 3. Back to Main Thread
        // Fetch RecyclerView
        // R.id.rate_list is the id of the RecyclerView on 'Fragment_home' fragment layout
        val rateRecyclerview = requireView().findViewById<RecyclerView>(R.id.rateList)

        // The adapter
        //val adapter = RateAdapter(requireContext(), R.layout.item_rate, rates)
        val adapter = RateAdapter(requireContext(), R.layout.item_rate, ratesAPI)

        // Associate the adapter to the RecyclerView
        rateRecyclerview.adapter = adapter

        // Set the Recyclerview's layout manager
        rateRecyclerview.layoutManager = LinearLayoutManager(requireContext())
        //rateRecyclerview.layoutManager = GridLayoutManager(requireContext(), 2)

        rateRecyclerview.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        )

    }

    // ---
    // DATA (RATES)
    // ---

    /**
     * Get a static rate list
     */
    private fun getRates(): MutableList<Rate>
    {
        val rateList = mutableListOf<Rate>()

        for(i in 1..30)
        {
            rateList.add(Rate(1,"EUR","EURO",1.0))
            rateList.add(Rate(2,"USD","United State Dollar",0.8))
            rateList.add(Rate(3,"AED","United Arab Emirates Dirham",3.75455))
        }

        return rateList
    }

    // ---
    // TESTS
    // ---
    private fun getRatesFromJson(): MutableList<Rate> {
        /// ---
        /// Currencies
        /// ---
        val rawCurrencies = IoUtil.readTextFile(requireContext(), "data/currencies.json")

        Log.d(AppConfig.TAG,rawCurrencies)

        val currencies = CurrencyConverter.convert(rawCurrencies)
        for(currency in currencies)
        {
            Log.d(AppConfig.TAG, currency.toString())
        }

        /// ---
        /// Rates
        /// ---
        val rawRates = IoUtil.readTextFile(requireContext(), "data/rates.json")

        val rates = RateConverter.convert(rawRates, currencies)
        for(rate in rates)
        {
            Log.d(AppConfig.TAG, rate.toString())
        }

        return rates
    }

    /**
     * Get rates from API
     */
    private fun getRatesFromAPI(): MutableList<Rate> {

        /// ---
        /// Currencies
        /// ---
        val rawCurrencies = HttpUtil.readUrl(AppConfig.CURRENCY_API)

        val currencies = CurrencyConverter.convert(rawCurrencies)

        /// ---
        /// Rates
        /// ---
        val rawRates = HttpUtil.readUrl(AppConfig.RATE_API)

        val rates = RateConverter.convert(rawRates, currencies)

        Log.d(AppConfig.TAG,"Starting the API")
        for(rate in rates)
        {
            //Log.d(AppConfig.TAG,  rate.toString())
        }
        Log.d(AppConfig.TAG,"Finishing the API")

       return rates
    }

    private fun testRates() {
        val rawRates = IoUtil.readTextFile(requireContext(), "data/rates.json")

        //Log.d(AppConfig.TAG,rawRates)
    }
}